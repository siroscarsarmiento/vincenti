<?php
namespace Neos\Flow\Persistence\Doctrine\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs! This block will be used as the migration description if getDescription() is not used.
 */
class Version20200613012359 extends AbstractMigration
{

    /**
     * @return string
     */
    public function getDescription()
    {
        return '';
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function up(Schema $schema)
    {
        // this up() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('CREATE TABLE evenpro_vincenti_domain_model_multimedia (persistence_object_identifier VARCHAR(40) NOT NULL, originalresource VARCHAR(40) DEFAULT NULL, iconoresource VARCHAR(40) DEFAULT NULL, adjuntoresource VARCHAR(40) DEFAULT NULL, titulo VARCHAR(255) NOT NULL, tituloen VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, vigente INT NOT NULL, UNIQUE INDEX UNIQ_5425F8A74E59BB9C (originalresource), UNIQUE INDEX UNIQ_5425F8A78FB51BFB (iconoresource), UNIQUE INDEX UNIQ_5425F8A7964B2A94 (adjuntoresource), PRIMARY KEY(persistence_object_identifier)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE evenpro_vincenti_domain_model_prensa (persistence_object_identifier VARCHAR(40) NOT NULL, originalresource VARCHAR(40) DEFAULT NULL, iconoresource VARCHAR(40) DEFAULT NULL, adjuntoresource VARCHAR(40) DEFAULT NULL, titulo VARCHAR(255) NOT NULL, tituloen VARCHAR(255) NOT NULL, fecha DATETIME NOT NULL, vigente INT NOT NULL, UNIQUE INDEX UNIQ_B01D35F64E59BB9C (originalresource), UNIQUE INDEX UNIQ_B01D35F68FB51BFB (iconoresource), UNIQUE INDEX UNIQ_B01D35F6964B2A94 (adjuntoresource), PRIMARY KEY(persistence_object_identifier)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_multimedia ADD CONSTRAINT FK_5425F8A74E59BB9C FOREIGN KEY (originalresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_multimedia ADD CONSTRAINT FK_5425F8A78FB51BFB FOREIGN KEY (iconoresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_multimedia ADD CONSTRAINT FK_5425F8A7964B2A94 FOREIGN KEY (adjuntoresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_prensa ADD CONSTRAINT FK_B01D35F64E59BB9C FOREIGN KEY (originalresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_prensa ADD CONSTRAINT FK_B01D35F68FB51BFB FOREIGN KEY (iconoresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
        $this->addSql('ALTER TABLE evenpro_vincenti_domain_model_prensa ADD CONSTRAINT FK_B01D35F6964B2A94 FOREIGN KEY (adjuntoresource) REFERENCES neos_flow_resourcemanagement_persistentresource (persistence_object_identifier)');
    }

    /**
     * @param Schema $schema
     * @return void
     */
    public function down(Schema $schema)
    {
        // this down() migration is autogenerated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on "mysql".');
        
        $this->addSql('DROP TABLE evenpro_vincenti_domain_model_multimedia');
        $this->addSql('DROP TABLE evenpro_vincenti_domain_model_prensa');
    }
}
