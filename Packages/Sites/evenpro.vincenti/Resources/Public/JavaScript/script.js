$(document).ready(function(){
	var imgItems = $('.slider li').length; // Numero de Slides
	var imgPos = 1;

	// Agregando paginacion --
	for(i = 1; i <= imgItems; i++){
		$('.pagination').append('<li><span class="fa fa-circle"></span></li>');
	} 
	//------------------------

	$('.slider li').hide(); // Ocultanos todos los slides
	$('.slider li:first').show(); // Mostramos el primer slide
	$('.pagination li:first').css({'color': '#CD6E2E'}); // Damos estilos al primer item de la paginacion

	// Ejecutamos todas las funciones
	$('.pagination li').click(pagination);
	$('.right span').click(nextSlider);
	$('.left span').click(prevSlider);

	//slider automatico
	//setInterval(function(){
	//nextSlider();
	//}, 6000);

	// FUNCIONES =========================================================

	function pagination(){
		var paginationPos = $(this).index() + 1; // Posicion de la paginacion seleccionada

		$('.slider li').hide(); // Ocultamos todos los slides
		$('.slider li:nth-child('+ paginationPos +')').fadeIn(); // Mostramos el Slide seleccionado

		// Dandole estilos a la paginacion seleccionada
		$('.pagination li').css({'color': '#858585'});
		$(this).css({'color': '#CD6E2E'});

		imgPos = paginationPos;

	}

	function nextSlider(){
		if( imgPos >= imgItems){imgPos = 1;} 
		else {imgPos++;}

		$('.pagination li').css({'color': '#858585'});
		$('.pagination li:nth-child(' + imgPos +')').css({'color': '#CD6E2E'});

		$('.slider li').hide(); // Ocultamos todos los slides
		$('.slider li:nth-child('+ imgPos +')').fadeIn(); // Mostramos el Slide seleccionado

	}

	function prevSlider(){
		if( imgPos <= 1){imgPos = imgItems;} 
		else {imgPos--;}

		$('.pagination li').css({'color': '#858585'});
		$('.pagination li:nth-child(' + imgPos +')').css({'color': '#CD6E2E'});

		$('.slider li').hide(); // Ocultamos todos los slides
		$('.slider li:nth-child('+ imgPos +')').fadeIn(); // Mostramos el Slide seleccionado
	}

	var deplegable = $('.submenu2').click(function(){
					$(this).children('ul').slideToggle();
					});

		if (screen.width<=860){
			document.deplegable;
			}

		$('ul').click(function(p){
				p.stopPropagation();
			});

});

$(document).ready(function(){

	$(window).scroll(function(){

		if($(window).scrollTop() > 40){

			$('.menu').addClass('menu2');
		}else{
				$('.menu').removeClass('menu2');
		}

		if($(window).scrollTop() > 40){

			$('.submenu2 > ul').addClass('submn2');
		}else{
				$('.submenu2 > ul').removeClass('submn2');
		}

		/*if($(window).scrollTop() > 40){

			$('.logo-p').addClass('logo-p4');
		}else{
				$('.logo-p').removeClass('logo-p4');
		}*/

		if($(window).scrollTop() > 40){

			$('.logo-p2').addClass('logo-p3');
		}else{
			$('.logo-p2').removeClass('logo-p3');
		}

		if($(window).scrollTop() > 40){

			$('.mn-hamb').addClass('mn-ham-rsp');
		}else{
			$('.mn-hamb').removeClass('mn-ham-rsp');
		}

		/*if($(window).scrollTop() > 40){

			$('.submenu2 a').addClass('btn-mn');
		}else{
			$('.submenu2 a').removeClass('btn-mn');
		}*/

		if($(window).scrollTop() > 40){

			$('.submenu2 a span i').addClass('btn-mn');
		}else{
			$('.submenu2 a span i').removeClass('btn-mn');
		}

		if($(window).scrollTop() > 40){

			$('.header3').addClass('header2');
		}else{
				$('.header3').removeClass('header2');
		}

		if($(window).scrollTop() > 40){

			$('header').addClass('header2');
		}else{
				$('header').removeClass('header2');
		}

		if($(window).scrollTop() > 40){

			$('.mn-productos').addClass('mn-productos2');
		}else{
				$('.mn-productos').removeClass('mn-productos2');
		}

		if($(window).scrollTop() > 2000){

			$('.social-media-header').addClass('social-none');
		}else{
				$('.social-media-header').removeClass('social-none');
		}

	});

	//CONTROLADORES DE TABS

	$('ul.tabs li a:first').addClass('active');
	$('.secciones article').hide();
	$('.secciones article:first').show();

	$('ul.tabs li a').click(function(){
		$('ul.tabs li a').removeClass('active');
		$(this).addClass('active');
		$('.secciones article').hide();

		var activeTab = $(this).attr('href');
		$(activeTab).show();
		return false;

	});
});

	$(document).ready(function()
	    {
	    $("#mn-hamb").on( "click", function() {	 
	        $('#menu-ppal').toggle("slow");
	         });
	    });