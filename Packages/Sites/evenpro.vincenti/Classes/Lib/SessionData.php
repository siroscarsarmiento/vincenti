<?php
namespace evenpro\vincenti\Lib;
use Neos\Flow\Annotations as Flow;
/**
* @Flow\Scope("session")
*/
class SessionData 
{       
        
	/**
	* @var array
	*/
	protected $items = array();
	/**
	* @param string $item
	* @return void
	* @Flow\Session(autoStart = TRUE)
	*/
	public function addItem($item) {
		$match=false;
		if(count($this->items)>0){
			foreach($item as $clave=>$valor){
				for($i=0;$i<=count($this->items)-1;$i++){
					if(array_key_exists($clave,$this->items[$i])){
						$this->items[$i][$clave]=$valor;
						$match=true;
					}
				}
			}
		}
		if(!$match){
			$this->items[] = $item;
		}
	}
	/**
	* @return array
	*/
	public function getItems() {
		return $this->items;
	}

	public function destroy(){
		$this->items =[];
	}
}