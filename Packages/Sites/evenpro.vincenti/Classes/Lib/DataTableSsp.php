<?php
namespace evenpro\vincenti\Lib;

/**
 * Summary.
 *
 * Description: Libreria PHP para datatables
 *
 * @since 1.0
 * @author Juan Blanco <jblanco@cinex.com.ve>
 * @copyright Envepro 2018
 */
/**
  * @method void __construct() Constructor
  * @method response responseAcceptHttp($parameters) Metodo de Respuesta afirmativa del objeto HTTP
  */ 
class DataTableSsp 
{       
        
    /**
	 * Perform the SQL queries needed for an server-side processing requested,
	 * utilising the helper functions of this class, limit(), order() and
	 * filter() among others. The returned array is ready to be encoded as JSON
	 * in response to an SSP request, or can be modified if needed before
	 * sending back to the client.
	 *
	 *  @param  array $request Data sent to server by DataTables
     *  @param  string $entityManager Entity Manager
	 *  @param  string $entity SQL table to query
	 *  @param  string $primaryKey Primary key of the table
	 *  @param  array $columns Column information array
	 *  @return array Server-side processing response array
	 */
	static function simple ( $request,$entityManager, $entity, $primaryKey, $columns, $whereOptional )
	{

    
        $query = $entityManager->createQueryBuilder();   
        $bindings = array();

        $parametros="";
        $where="";
        $limit="";
        $order="";
		
		//$db = self::db( $conn );
		// Build the SQL query string from the request
		$limit = self::limit( $request, $columns );
		$order = self::order( $request, $columns );
		$where = self::filter( $request, $columns, $bindings );
        // Main query to actually get the data
       // $buzon = $buzonRepository->findCorreoByUsuarioId($limit,$order,$where);
		
       foreach($columns as $valor){
            $query->addSelect($valor["db"]);
       }
       
       $query->setFirstResult($limit[0]);
	   $query->setMaxResults($limit[1]);  
	   for($i=0;$i<count($entity);$i++){
			if ($i==0){
				$query->from($entity[$i][0], $entity[$i][1]);
				if(isset($entity[$i][2])){
					if($entity[$i][4]=="innerjoin"){	
						$query->Join($entity[$i][2], $entity[$i][3]);					
					}elseif($entity[$i][4]=="leftjoin"){
						$query->leftJoin($entity[$i][2], $entity[$i][3]);
					}
				}	
			}
		}
	


       //$query->from($entity, 'c');
	   //$query->leftJoin('c.buzonglobal', 'g');
       $cont=0;
       foreach($where as $valor){
           if($where==0){
                $query->where($valor[0]. " like '%" .$valor[1]. "%'");
           }else{
                $query->orWhere($valor[0]. " like '%" .$valor[1]. "%'");
           }
       }
    
       foreach($whereOptional as $valor){
            if(count($where)>0){
                if($valor["operadorlogico"]=="and"){
                    $criterio = $valor['campo'];
                    $criterio .= $valor['operador'];
                    $criterio .= $valor["tipodato"]=="integer" ? $valor["valor"] : "'".$valor["valor"]."'";
                    
                    $query->andWhere($criterio);
                }elseif($valor["operadorlogico"]=="or"){
                    $criterio = $valor['campo'];
                    $criterio .= $valor['operador'];
                    $criterio .= $valor["tipodato"]=="integer" ? $valor["valor"] : "'".$valor["valor"]."'";
                    $query->orWhere($criterio);
                }
            }    
       }
	 
		if(count($order)>0){
			$query->addOrderBy($order[0], $order[1]);
		}
       $data = $query->getQuery()->execute();
       $query = $entityManager->createQueryBuilder(); 
       $query->select('count('.$primaryKey.') as total');
	   /*$query->from($entity,'c');
	   $query->leftJoin('c.buzonglobal', 'g');*/
	   for($i=0;$i<count($entity);$i++){
			if ($i==0){
				$query->from($entity[$i][0], $entity[$i][1]);
				if(isset($entity[$i][2])){
					if($entity[$i][4]=="innerjoin"){	
						$query->Join($entity[$i][2], $entity[$i][3]);					
					}elseif($entity[$i][4]=="leftjoin"){
						$query->leftJoin($entity[$i][2], $entity[$i][3]);
					}
				}	
			}
	   }
       foreach($where as $valor){
            if($where==0){
                $query->where($valor[0]. " like '%" .$valor[1]. "%'");
            }else{
                $query->orWhere($valor[0]. " like '%" .$valor[1]. "%'");
            }
        }
       $records = $query->getQuery()->getSingleScalarResult();
       $recordsFiltered =(integer)$records;

       $query= null;
       $query = $entityManager->createQueryBuilder(); 
       $query->select('count('.$primaryKey.') as total');
	  /* $query->from($entity,'c');
	   $query->leftJoin('c.buzonglobal', 'g');*/
	   for($i=0;$i<count($entity);$i++){
			if ($i==0){
				$query->from($entity[$i][0], $entity[$i][1]);
				if(isset($entity[$i][2])){
					if($entity[$i][4]=="innerjoin"){	
						$query->Join($entity[$i][2], $entity[$i][3]);					
					}elseif($entity[$i][4]=="leftjoin"){
						$query->leftJoin($entity[$i][2], $entity[$i][3]);
					}
				}	
			}
	   }
       $resTotalLength  = $query->getQuery()->getSingleScalarResult();
       $recordsTotal =(integer)$resTotalLength ;

	   $draw = $request->getArguments()['draw'];  
	    
       return array(
            "draw"=> isset ( $draw ) ?
                intval( $draw ) :
                0,
            "recordsTotal"    => intval($recordsTotal) ,
            "recordsFiltered" => intval( $recordsFiltered ),
            "data"            => self::data_output( $columns, $data )
        );


       // return $data;

	}

    /**
	 * Paging
	 *
	 * Construct the LIMIT clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL limit clause
	 */
	static function limit ( $request, $columns )
	{
        $limit = '';
        $start=$request->getArguments()['start'];
		if ( isset($start) && $request->getArguments()['length'] != -1 ) {
			$limit = array(intval($start),intval($request->getArguments()['length']));
		}
		return $limit;
	}
       
    	/**
	 * Ordering
	 *
	 * Construct the ORDER BY clause for server-side processing SQL query
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *  @return string SQL order by clause
	 */
	static function order ( $request, $columns )
	{
        $order = '';
		$orderArray=[];
		$order=$request->getArguments()['order'];
		
		$columnSearch= $request->getArguments()['columns'];
        if ( isset($order) && count($order) ) {
		
			//if(!(strpos($columns[$order[0]["column"]]["db"],"AS")) and (!strpos($columns[$order[0]["column"]]["db"],"as"))){

			if($columnSearch[$columns[$order[0]["column"]]["dt"]]["searchable"]=="true"){	
				$dir = $order[0]["dir"] === 'asc' ? "asc" : "desc";
                $orderArray = array($columns[$order[0]["column"]]["db"] , $dir);
			}else{
				//$dir = $order[0]["dir"] === 'asc' ? "asc" : "desc";
				$orderArray = array("c.Persistence_Object_Identifier" , "Asc");
				//$order = 'order by 0 '.$order[0]["dir"];	
			}
		
		}
		return  $orderArray;
    }
    
    /**
	 * Searching / Filtering
	 *
	 * Construct the WHERE clause for server-side processing SQL query.
	 *
	 * NOTE this does not match the built-in DataTables filtering which does it
	 * word by word on any field. It's possible to do here performance on large
	 * databases would be very poor
	 *
	 *  @param  array $request Data sent to server by DataTables
	 *  @param  array $columns Column information array
	 *    sql_exec() function
	 *  @return string SQL where clause
	 */
	static function filter ( $request, $columns )
	{
		$globalSearch=array();
        $columnSearch= $request->getArguments()['columns'];
        $search= $request->getArguments()['search'];
        foreach($columnSearch as $clave=>$valor){
             if($valor["searchable"]=="true"){  
                //if(!(strpos($columns[$valor['data']]['db'],"AS")) and (!strpos($columns[$valor['data']]['db'],"as"))){
					if(!empty($search["value"])){
						$globalSearch[] = array($columns[$valor['data']]['db'],$search["value"]);  
					} 	
                //}
             }
        }
		return $globalSearch;
    }
    

	/**
	 * Pull a particular property from each assoc. array in a numeric array, 
	 * returning and array of the property values from each item.
	 *
	 *  @param  array  $a    Array to get data from
	 *  @param  string $prop Property to read
	 *  @return array        Array of property values
	 */
	static function pluck ( $a, $prop )
	{
		$out = array();
		for ( $i=0, $len=count($a) ; $i<$len ; $i++ ) {
			$out[] = $a[$i][$prop];
		}
		return $out;
    }
    

    /**
	 * Create the data output array for the DataTables rows
	 *
	 *  @param  array $columns Column information array
	 *  @param  array $data    Data from the SQL get
	 *  @return array          Formatted data in a row based format
	 */
	static function data_output ( $columns, $data )
	{
        $out = array();
		for ( $i=0, $ien=count($data) ; $i<$ien ; $i++ ) {
			$row = array();
			for ( $j=0, $jen=count($columns) ; $j<$jen ; $j++ ) {
				$column = $columns[$j];
				// Is there a formatter?
				if ( isset( $column['formatter'] ) ) {
					$row[ $column['dt'] ] = $column['formatter']( $data[$i][ $column['nombreindice'] ], $data[$i] );
				}
				else {
                    $row[ $column['dt'] ] =$data[$i][$columns[$j]['nombreindice']];
				}
			}
			$out[] = $row;
		}
		return $out;
	}

}