<?php
namespace evenpro\vincenti\Controller;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Mvc\Routing\UriBuilder;
use Neos\Flow\ResourceManagement\ResourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use evenpro\vincenti\Domain\Model\Contacto;
use evenpro\vincenti\Domain\Model\Farmacovigilancia;

class ContactoController extends ActionController 
{
    /**
     * Inject the settings
     *
     * @param array $settings
     * @return void
     */
    public function injectSettings(array $settings) {
        $this->settings = $settings;
    }

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\ContactoRepository
     */
    protected $contactoRepository;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\FarmacovigilanciaRepository
     */
    protected $farmacovigilanciaRepository;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;


    /**
     * @return string
     */
    public function indexAction()
    {
        $this->view->assign('data',array("data"=>[],'dominio'=>$this->dominio));
    }

    /**
     * @return string
     */
    public function indexInglesAction()
    {
        $this->view->assign('data',array("data"=>[],'dominio'=>$this->dominio));
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Contacto $newContacto
     * @return void
     */
    public function createAction()
    {

        $mail = new \Neos\SwiftMailer\Message();
        #$mailContacto = new \Neos\SwiftMailer\Message();

        $httpRequest = $this->request->getHttpRequest();
        $name = $this->request->getHttpRequest()->getArgument('name');
        $email = $this->request->getHttpRequest()->getArgument('email');
        $telefono = $this->request->getHttpRequest()->getArgument('telefono');
        $mensaje = $this->request->getHttpRequest()->getArgument('mensaje');
        $contacto = new contacto;
        $contacto->setNombre($name);
        $contacto->setCorreo($email);
        $contacto->setTelefono($telefono);
        $contacto->setMensaje($mensaje);
        $this->contactoRepository->add($contacto);
        $this->persistenceManager->persistAll();

        $mail
            ->setFrom(array("info@vincentilab.com"=> "info@vincentilab.com"))
            ->setTo(array($email,'info@vincentilab.com' => $email))
            ->setSubject("Vincenti CONTACTO");
        $mail->setBody($mensaje, 'text/html');
        $correoAlAdmin= $mail->send();    
        return json_encode(array("result"=> "Enviado Exitosamente"));
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Farmacovigilancia $newFarmacovigilancia
     * @return void
     */
    public function createFarmacovigilanciaAction()
    {
        $mail = new \Neos\SwiftMailer\Message();

        $httpRequest = $this->request->getHttpRequest();
        $nombre = $this->request->getHttpRequest()->getArgument('nombre');
        $ci = $this->request->getHttpRequest()->getArgument('ci');
        $correo = $this->request->getHttpRequest()->getArgument('correo');
        $telefono = $this->request->getHttpRequest()->getArgument('telefono');
        $medicamento = $this->request->getHttpRequest()->getArgument('medicamento');
        $presentacion = $this->request->getHttpRequest()->getArgument('presentacion');
        $reaccion = $this->request->getHttpRequest()->getArgument('reaccion');
        $coments = $this->request->getHttpRequest()->getArgument('coments');
        $contacto = new farmacovigilancia;
        $contacto->setNombre($nombre);
        $contacto->setCi($ci);
        $contacto->setCorreo($correo);
        $contacto->setTelefono($telefono);
        $contacto->setMedicamento($medicamento);
        $contacto->setPresentacion($presentacion);
        $contacto->setReaccion($reaccion);
        $contacto->setComents($coments);
        $this->farmacovigilanciaRepository->add($contacto);
        $this->persistenceManager->persistAll();

        // $mensaje = "Enviado por " . $nombre . "\r\n";
        // $mensaje.= "Cédula " . $ci . "\r\n";
        // $mensaje.= "Teléfono " . $telefono . "\r\n";
        // $mensaje.= "Nombre del Medicamento " .  $medicamento . "\r\n";
        // $mensaje.= "Presentación " .  $presentacion . " \r\n";
        // $mensaje.= "Reacción Presentada " .  $reaccion . " \r\n";
        // $mensaje.= "Comentario " .  $coments;
        
        $mail
            ->setFrom(array("info@vincentilab.com"=> "info@vincentilab.com"))
            ->setTo(array($correo,'info@vincentilab.com' => $correo))
            ->setSubject("Vincenti Farmacovigilancia");
        $mail->setBody(
            '<html>' .
            ' <body>' .
            '  Enviado por <H3>' . $nombre . ' </H3>' .
            '<hr>' .
            '  Cédula <H3>' . $ci . ' </H3>' .
            '<hr>' .
            '  Teléfono <H3>' . $telefono . ' </H3>' .
            '<hr>' .
            '  Nombre del Medicamento <H3>' . $medicamento . ' </H3>' .
            '<hr>' .
            '  Presentación <H3>' . $presentacion . ' </H3>' .
            '<hr>' .
            '  Reacción Presentada <H3>' . $reaccion . ' </H3>' .
            '<hr>' .
            '  Comentario <H3>' . $coments . ' </H3>' .
            ' </body>' .
            '</html>',
              'text/html' 
            );
        $correoAlAdmin= $mail->send();    

        return json_encode(array("result"=> "Enviado Exitosamente"));
       // $this->redirect('index');
    }
}
