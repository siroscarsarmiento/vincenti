<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

use Neos\Flow\ResourceManagement\ResourceManager;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class FarmacovigilanciaController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;


    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;


    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('dominio', $this->dominio);
    }




}
