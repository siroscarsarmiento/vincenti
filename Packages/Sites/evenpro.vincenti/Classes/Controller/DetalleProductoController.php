<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the evenpro.backstage package.
 */

use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Domain\Model\Producto;
use evenpro\vincenti\Lib\DataTableSsp;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DetalleProductoController extends ActionController
{


  /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\ProductoRepository
     */
    protected $productoRepository;


     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;


    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;


    /**
     * @return void
     */
    public function indexAction()
    {
        $idProducto = $this->request->getHttpRequest()->getArgument("idproducto");
        $data=$this->productoRepository->findByIdProducto($idProducto);
        $this->view->assign('data',array("data"=>$data));
          //$idProducto = $this->request->getHttpRequest()->getArgument("idproducto");
           // var_dump($idProducto);
    }

    /**
     * @return void
     */
    public function indexDetalleEnglishAction()
    {
        $idProducto = $this->request->getHttpRequest()->getArgument("idproducto");
        $data=$this->productoRepository->findByIdProducto($idProducto);
        $this->view->assign('data',array("data"=>$data));
    }
}
