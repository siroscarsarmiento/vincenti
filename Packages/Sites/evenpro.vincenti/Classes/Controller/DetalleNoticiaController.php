<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the evenpro.backstage package.
 */

use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Domain\Model\Producto;
use evenpro\vincenti\Lib\DataTableSsp;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;

class DetalleNoticiaController extends ActionController
{


  /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\NoticiasRepository
     */
    protected $noticiaRepository;


     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;


    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;


    /**
     * @return void
     */
    public function indexAction()
    {
        $idNoticia= $this->request->getHttpRequest()->getArgument("idnoticia");
        $data=$this->noticiaRepository->findById($idNoticia);
        $this->view->assign('data',array("data"=>$data));
          //$idProducto = $this->request->getHttpRequest()->getArgument("idproducto");
           // var_
         
    }

    /**
     * @return void
     */
    public function indexDetalleEnglishAction()
    {
        $idNoticia= $this->request->getHttpRequest()->getArgument("idnoticia");
        $data=$this->noticiaRepository->findById($idNoticia);
        $this->view->assign('data',array("data"=>$data));
          //$idProducto = $this->request->getHttpRequest()->getArgument("idproducto");
           // var_
         
    }
    
}
