<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Domain\Model\Categoria;
use evenpro\vincenti\Lib\DataTableSsp;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class CategoriaController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\CategoriaRepository
     */
    protected $categoriaRepository;


     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;


    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('dominio', $this->dominio);
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Categoria $categoria
     * @return void
     */
    public function showAction(Categoria $categoria)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('categoria', $categoria);
    }

    /**
     * @return void
     */
    public function newAction()
    {
        $this->view->assign('dominio', $this->dominio);
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Categoria $newCategoria
     * @return void
     */
    public function createAction(Categoria $newCategoria)
    {
        $this->categoriaRepository->add($newCategoria);
        //$this->addFlashMessage('Nuevo Producto Creado.');
        //return true;
        $this->redirect('index');
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Categoria $categoria
     * @return void
     */
    public function editAction($id)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('categoria', $id);
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Categoria $categoria
     * @return void
     */
    public function updateAction(Categoria $categoria)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->categoriaRepository->update($categoria);
        //$this->addFlashMessage('Producto actualizado.');
         $this->redirect('index');
        return true;
    }



 /**
    * This action outputs a custom greeting
    *
    * @param string $id 
    * @return string confirmacion
    */
    public function deleteAction($id){
        $this->view->assign('dominio', $this->dominio);
        $data=$this->categoriaRepository->findById($id);
        $this->categoriaRepository->remove($data[0]);
        return json_encode(array("eliminado"=>"true"));
    }

    /**
    * This action outputs a custom greeting
    *
    * @return string custom greeting
    */
    public function dataAction(){
       $this->view->assign('dominio', $this->dominio);
       $httpRequest = $this->request->getHttpRequest();
       $entity[0]=array('\evenpro\vincenti\Domain\Model\Categoria',"c");
       $primaryKey = 'c.Persistence_Object_Identifier';      
           $whereOptional = array();
           $columns = array(
               array( 'db' => 'c.categoria','dt' => 0,'alias'=>false,"nombreindice"=>"categoria"),
               array( 'db' => 'c.publicar','dt' => 1,'alias'=>false,"nombreindice"=>"publicar", 'formatter' => function( $d, $row ) {
                if($d==1){
                    return "<i class='fas fa-check' style='cursor: pointer'></i>"; 
                };
               }),
               array( 'db' => 'c.Persistence_Object_Identifier','dt' => 2,'alias'=>false,"nombreindice"=>"Persistence_Object_Identifier" ),
                array( 'db' => '\'x\' as Edit','dt' => 3,'alias'=>true,"nombreindice"=>"Edit",'formatter' => function( $d, $row ) {
                    return "<i class='fas fa-edit' style='cursor: pointer'></i>";
                  }),
                array( 'db' => '\'x\' as Sel','dt' => 4,'alias'=>true,"nombreindice"=>"Sel",'formatter' => function( $d, $row ) {
                    return "<i class='fas fa-trash-alt' style='cursor: pointer'></i>";
                  }),
           );
           $data = $this->libDataTable->simple($httpRequest,$this->entityManager, $entity, $primaryKey, $columns,$whereOptional);

           return json_encode($data); 
    }
}
