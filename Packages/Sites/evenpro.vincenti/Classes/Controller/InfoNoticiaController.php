<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

/*use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Domain\Model\Producto;*/
use Neos\Flow\ResourceManagement\ResourceManager;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;
use evenpro\vincenti\Domain\Model\Noticias;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class InfoNoticiaController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\NoticiasRepository
     */
    protected $noticiasRepository;




    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;



    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $dataNoticia = [];
        $data=$this->noticiasRepository->findAll();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
               
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataNoticia[$clave]["ImageProducto"]=$resourceUri;
                $dataNoticia[$clave]["titulo"]=$valor->getTitulo();
                $dataNoticia[$clave]["resumen"]=$valor->getResumen();
                $dataNoticia[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }

            //var_dump($dataNoticia);
            //die();
        }

        $this->view->assign('data',array("data"=>$dataNoticia));
    }

    
    /**
     * @return void
     */
    public function indexEnglishAction()
    {
        $dataNoticia = [];
        $data=$this->noticiasRepository->findAll();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataNoticia[$clave]["ImageProducto"]=$resourceUri;
                $dataNoticia[$clave]["titulo"]=$valor->getTituloEn();
                $dataNoticia[$clave]["resumen"]=$valor->getResumenEn();
                $dataNoticia[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }

            //var_dump($dataNoticia);
            //die();
        }

        $this->view->assign('data',array("data"=>$dataNoticia));
    }


}
