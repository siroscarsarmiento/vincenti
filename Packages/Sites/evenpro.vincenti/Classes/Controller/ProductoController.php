<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

/*use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Domain\Model\Producto;*/
use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Domain\Model\Producto;
use evenpro\vincenti\Domain\Model\Categoria;
use evenpro\vincenti\Domain\Repository\CategoriaRepository;
use evenpro\vincenti\Lib\DataTableSsp;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class ProductoController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\ProductoRepository
     */
    protected $productoRepository;

    /**
     * @Flow\Inject
     * @var CategoriaRepository
     */
    protected $categoriaRepository;


     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;



    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('dominio', $this->dominio);
        $httpRequest = $this->request->getHttpRequest();
        if(count($this->session->getItems())>2){
            $token=$this->session->getItems()[0]["token"];
        }else{
            $token="";            
        }
       // $this->view->assign('data',array("data"=>$httpRequest->getArguments()["email"],"token"=>$token));
        
        $this->view->assign('producto',array("token"=>$token));
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Producto $producto
     * @return void
     */
    public function showAction(Producto $producto)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('producto', $producto);
    }

    /**
     * @return void
     */
    public function newAction()
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('categorias', $this->categoriaRepository->findAll());
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Producto $newProducto
     * @return void
     */
    public function createAction(Producto $newProducto)
    {
        //var_dump($newProducto); die();
        $this->view->assign('dominio', $this->dominio);
        $this->productoRepository->add($newProducto);
        //$this->addFlashMessage('Nuevo Producto Creado.');
        //return true;
        $this->redirect('index');
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Producto $producto
     * @return void
     */
    public function editAction($id)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('producto', $id);
        $this->view->assign('categorias', $this->categoriaRepository->findAll());
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Producto $producto
     * @return void
     */
    public function updateAction(Producto $producto)
    {
       // $this->view->assign('dominio', $this->dominio);
        $this->productoRepository->update($producto);
        //$this->addFlashMessage('Producto actualizado.');
         $this->redirect('index');
        return true;
    }



 /**
    * This action outputs a custom greeting
    *
    * @param string $id 
    * @return string confirmacion
    */
    public function deleteAction($id){
        $this->view->assign('dominio', $this->dominio);
        $data=$this->productoRepository->findById($id);
        var_dump($data); die();
        $this->productoRepository->remove($data[0]);
        return json_encode(array("eliminado"=>"true"));
    }

    /**
    * This action outputs a custom greeting
    *
    * @return string custom greeting
    */
    public function dataAction(){
       $this->view->assign('dominio', $this->dominio);
       $httpRequest = $this->request->getHttpRequest();
       $entity[0]=array('\evenpro\vincenti\Domain\Model\Producto',"c");
       $primaryKey = 'c.Persistence_Object_Identifier';      
           $whereOptional = array();
           $columns = array(
               array( 'db' => 'c.nombre','dt' => 0,'alias'=>false,"nombreindice"=>"nombre"),
               array( 'db' => 'c.descripcion','dt' => 1,'alias'=>false,"nombreindice"=>"descripcion"),
               array( 'db' => 'c.publicar','dt' => 2,'alias'=>false,"nombreindice"=>"publicar", 'formatter' => function( $d, $row ) {
                if($d==1){
                    return "<i class='fas fa-check' style='cursor: pointer'></i>"; 
                };
               }),
               array( 'db' => 'c.Persistence_Object_Identifier','dt' => 3,'alias'=>false,"nombreindice"=>"Persistence_Object_Identifier" ),
                array( 'db' => '\'x\' as Edit','dt' => 4,'alias'=>true,"nombreindice"=>"Edit",'formatter' => function( $d, $row ) {
                    return "<i class='fas fa-edit' style='cursor: pointer'></i>";
                  }),
                array( 'db' => '\'x\' as Sel','dt' => 5,'alias'=>true,"nombreindice"=>"Sel",'formatter' => function( $d, $row ) {
                    return "<i class='fas fa-trash-alt' style='cursor: pointer'></i>";
                  }),
           );
           $data = $this->libDataTable->simple($httpRequest,$this->entityManager, $entity, $primaryKey, $columns,$whereOptional);

           return json_encode($data); 
    }
}
