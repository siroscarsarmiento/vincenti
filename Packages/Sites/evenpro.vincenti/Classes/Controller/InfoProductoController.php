<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

/*use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Domain\Model\Producto;*/
use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Domain\Model\Producto;
use evenpro\vincenti\Domain\Model\Categoria;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class InfoProductoController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\ProductoRepository
     */
    protected $productoRepository;


    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\CategoriaRepository
     */
    protected $categoriaRepository;


    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;



    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $dataProducto=array();
        $idCategoria = $this->request->getHttpRequest()->getArgument("idcategoria");
        if(isset($idCategoria)){
            $data=$this->productoRepository->findByCategoria($idCategoria);    
        }else{
            $data=$this->productoRepository->findAllItems();
        }
        foreach($data as $clave=>$valor){
            if($data[0]->getOriginalResource()){
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataProducto[$clave]["ImageProducto"]=$resourceUri;
                $dataProducto[$clave]["nombre"]=$valor->getNombre();
                $dataProducto[$clave]["descripcion"]=$valor->getDescripcion();
                $dataProducto[$clave]["id"]=$valor->getPersistence_Object_Identifier();
            }
        }
        $categoria=$this->categoriaRepository->findAll();
        $this->view->assign('data',array("data"=>$dataProducto,"categoria"=>$categoria,'dominio'=>$this->dominio,'idCategoria'=>$idCategoria));

    }

    /**
     * @return void
     */
    public function indexEnglishAction()
    {
        $dataProducto=array();
        $idCategoria = $this->request->getHttpRequest()->getArgument("idcategoria");
        if(isset($idCategoria)){
            $data=$this->productoRepository->findByCategoria($idCategoria);    
        }else{
            $data=$this->productoRepository->findAllItems();
        }
        foreach($data as $clave=>$valor){
            if($data[0]->getOriginalResource()){
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataProducto[$clave]["ImageProducto"]=$resourceUri;
                $dataProducto[$clave]["nombre"]=$valor->getNombre();
                $dataProducto[$clave]["descripcion"]=$valor->getDescripcion();
                $dataProducto[$clave]["id"]=$valor->getPersistence_Object_Identifier();
            }
        }
        $categoria=$this->categoriaRepository->findAll();
        $this->view->assign('data',array("data"=>$dataProducto,"categoria"=>$categoria,'dominio'=>$this->dominio,'idCategoria'=>$idCategoria));

    }

}
