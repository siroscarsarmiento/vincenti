<?php
namespace evenpro\vincenti\Controller;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Mvc\Routing\UriBuilder;
use Neos\Flow\ResourceManagement\ResourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use evenpro\vincenti\Lib\SessionData;

class CintilloNoticiasController   extends ActionController 
{
    /**
     * Inject the settings
     *
     * @param array $settings
     * @return void
     */
    public function injectSettings(array $settings) {
        $this->settings = $settings;
    }



    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\NoticiasRepository
     */
    protected $noticiasRepository;

     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @return string
     */
    public function indexAction()
    {
        // $data=$this->noticiasRepository->findPublicados();
        // $this->view->assign('data',array("data"=>$data));

        $dataNoticia = [];
        $data=$this->noticiasRepository->findPublicados();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
               
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataNoticia[$clave]["ImageProducto"]=$resourceUri;
                $dataNoticia[$clave]["titulo"]=$valor->getTitulo();
                $dataNoticia[$clave]["resumen"]=$valor->getResumen();
                $dataNoticia[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }
            $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($data[0]->getOriginalResource());            
            $array["response"][$clave]["image"]=$resourceUri;
            //var_dump($dataNoticia);
            //die();
        }

        $this->view->assign('data',array("data"=>$dataNoticia));
    }

    /**
     * @return string
     */
    public function indexInglesAction()
    {

        $dataNoticia = [];
        $data=$this->noticiasRepository->findPublicados();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
               
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataNoticia[$clave]["ImageProducto"]=$resourceUri;
                $dataNoticia[$clave]["tituloEn"]=$valor->getTituloEn();
                $dataNoticia[$clave]["resumenEn"]=$valor->getResumenEn();
                $dataNoticia[$clave]["id"]=$valor->getPersistence_Object_Identifier();

            }
            $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($data[0]->getOriginalResource());            
            $array["response"][$clave]["image"]=$resourceUri;
        }

        $this->view->assign('data',array("data"=>$dataNoticia));
    }
}
