<?php
namespace evenpro\vincenti\Controller;
use Neos\Flow\Annotations as Flow;
use Neos\Error\Messages\Message;
use Neos\Flow\Mvc\Controller\ActionController;
use Neos\Flow\Mvc\Routing\UriBuilder;
use Neos\Flow\ResourceManagement\ResourceManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;
use evenpro\vincenti\Lib\SessionData;

class CarouselProductosController  extends ActionController 
{
    /**
     * Inject the settings
     *
     * @param array $settings
     * @return void
     */
    public function injectSettings(array $settings) {
        $this->settings = $settings;
    }



    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\ProductoRepository
     */
    protected $productoRepository;

     /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @Flow\Inject
     * @var ResourceManager
     */
    protected $resourceManager;

    /**
     * @return string
     */
    public function indexAction()
    {
        $dataProducto = [];
        $data=$this->productoRepository->findPublicados();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
               
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataProducto[$clave]["ImageProducto"]=$resourceUri;
                $dataProducto[$clave]["nombre"]=$valor->getNombre();
                $dataProducto[$clave]["descripcion"]=$valor->getDescripcion();
                $dataProducto[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }
            $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($data[0]->getOriginalResource());            
            $array["response"][$clave]["image"]=$resourceUri;
            
        }
        //var_dump($dataProducto);
        //die();
        $this->view->assign('data',array("data"=>$dataProducto));
    }

    /**
     * @return string
     */
    public function indexInglesAction()
    {
        $dataProducto = [];
        $data=$this->productoRepository->findPublicados();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
               
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataProducto[$clave]["ImageProducto"]=$resourceUri;
                $dataProducto[$clave]["nombreEn"]=$valor->getNombreEn();
                $dataProducto[$clave]["descripcionEn"]=$valor->getDescripcionEn();
                $dataProducto[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }
            $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($data[0]->getOriginalResource());            
            $array["response"][$clave]["image"]=$resourceUri;
            
        }
        //var_dump($dataProducto);
        //die();
        $this->view->assign('data',array("data"=>$dataProducto));
    }
}
