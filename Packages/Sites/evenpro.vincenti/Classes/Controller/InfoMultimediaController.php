<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the Producto package.
 */

/*use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Domain\Model\Producto;*/
use Neos\Flow\ResourceManagement\ResourceManager;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Lib\SessionData;
use evenpro\vincenti\Domain\Model\Multimedia;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;



class InfoMultimediaController extends ActionController
{
    /**
     * @Flow\Inject
     * @var \Neos\Flow\ResourceManagement\ResourceManager
     */
    protected $resourceManager;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\MultimediaRepository
     */
    protected $multimediaRepository;




    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;



    /**
     * @Flow\Inject
     * @var SessionData
     */
    protected $session;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $dataMultimedia = [];
        $data=$this->multimediaRepository->findAll();
        foreach($data as $clave=>$valor){
            if($data[0]->getOriginalResource()){
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $iconoUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getIconoResource());
                $adjuntoUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getAdjuntoResource());
                $dataMultimedia[$clave]["ImagePrensa"]=$resourceUri;
                $dataMultimedia[$clave]["IconoPrensa"]=$iconoUri;
                $dataMultimedia[$clave]["AdjuntoPrensa"]=$adjuntoUri;
                $dataMultimedia[$clave]["titulo"]=$valor->getTitulo();
                $dataMultimedia[$clave]["id"]=$valor->getPersistence_Object_Identifier();
               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }
            //var_dump($dataNoticia);
            //die();
        }

        $this->view->assign('data',array("data"=>$dataMultimedia));
    }

    
    /**
     * @return void
     */
    public function indexEnglishAction()
    {
        $dataNoticia = [];
        $data=$this->noticiasRepository->findAll();
        foreach($data as $clave=>$valor){
      
            if($data[0]->getOriginalResource()){
                $resourceUri = $this->resourceManager->getPublicPersistentResourceUri($valor->getOriginalResource());
                $dataNoticia[$clave]["ImageProducto"]=$resourceUri;
                $dataNoticia[$clave]["titulo"]=$valor->getTituloEn();
                $dataNoticia[$clave]["resumen"]=$valor->getResumenEn();
                $dataNoticia[$clave]["id"]=$valor->getPersistence_Object_Identifier();

               // $dataEvento[$clave]["imageEvento"]=$resourceUri;
            }

            //var_dump($dataNoticia);
            //die();
        }

        $this->view->assign('data',array("data"=>$dataNoticia));
    }


}
