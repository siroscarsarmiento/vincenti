<?php
namespace evenpro\vincenti\Controller;

/*
 * This file is part of the evenpro.vincenti package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Mvc\Controller\ActionController;
use evenpro\vincenti\Domain\Model\Noticias;

use Neos\Flow\ResourceManagement\ResourceManager;
use evenpro\vincenti\Lib\SessionData;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;


class NoticiasController extends ActionController
{

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Domain\Repository\NoticiasRepository
     */
    protected $noticiasRepository;

    /**
     * @Flow\Inject
     * @var \evenpro\vincenti\Lib\DataTableSsp
     */
    protected $libDataTable;

    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @Flow\InjectConfiguration(package="evenpro.vincenti", path="configuracion.dominio")
     * @var string
     */
    protected $dominio;

    /**
     * @return void
     */
    public function indexAction()
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('noticias', $this->noticiasRepository->findAll());
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Noticias $noticias
     * @return void
     */
    public function showAction(Noticias $noticias)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('noticias', $noticias);
    }

    /**
     * @return void
     */
    public function newAction()
    {
        $this->view->assign('dominio', $this->dominio);
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Noticias $newNoticias
     * @return void
     */
    public function createAction(Noticias $newNoticias)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->noticiasRepository->add($newNoticias);
        //$this->addFlashMessage('Created a new noticias.');
        $this->redirect('index');
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Noticias $noticias
     * @return void
     */
    public function editAction($id)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->view->assign('noticias', $id);
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Noticias $noticias
     * @return void
     */
    public function updateAction(Noticias $noticias)
    {
        $this->view->assign('dominio', $this->dominio);
        $this->noticiasRepository->update($noticias);
        //$this->addFlashMessage('Updated the noticias.');
        $this->redirect('index');
    }

    /**
     * @param \evenpro\vincenti\Domain\Model\Noticias $noticias
     * @return void
     */
    public function deleteAction($id)
    {
        $this->view->assign('dominio', $this->dominio);
        $data=$this->noticiasRepository->findById($id);
        $this->noticiasRepository->remove($data[0]);
        return json_encode(array("eliminado"=>"true"));
    }

    /**
    * This action outputs a custom greeting
    *
    * @return string custom greeting
    */
    public function dataAction(){
        $this->view->assign('dominio', $this->dominio);
        $httpRequest = $this->request->getHttpRequest();
        $entity[0]=array('\evenpro\vincenti\Domain\Model\Noticias',"c");
        $primaryKey = 'c.Persistence_Object_Identifier';      
            $whereOptional = array();
            $columns = array(
                array( 'db' => 'c.titulo','dt' => 0,'alias'=>false,"nombreindice"=>"titulo"),
                array( 'db' => 'c.fecha','dt' => 1,'alias'=>false, "nombreindice"=>"fecha", 'formatter' => function( $d, $row ) {
                    return $d->format("d/m/Y");
                }),
                 array( 'db' => 'c.Persistence_Object_Identifier','dt' => 2,'alias'=>false,"nombreindice"=>"Persistence_Object_Identifier" ),
                 array( 'db' => '\'x\' as Edit','dt' => 3,'alias'=>true,"nombreindice"=>"Edit",'formatter' => function( $d, $row ) {
                     return "<i class='fas fa-edit' style='cursor: pointer'></i>";
                   }),
                 array( 'db' => '\'x\' as Sel','dt' => 4,'alias'=>true,"nombreindice"=>"Sel",'formatter' => function( $d, $row ) {
                     return "<i class='fas fa-trash-alt' style='cursor: pointer'></i>";
                   }),
            );
            $data = $this->libDataTable->simple($httpRequest,$this->entityManager, $entity, $primaryKey, $columns,$whereOptional);
 
            return json_encode($data); 
     }
 
}
