<?php
namespace evenpro\vincenti\Domain\Repository;

/*
 * This file is part of the form.poster package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;
/**
 * @Flow\Scope("singleton")
 */
class ProductoRepository extends Repository
{

     /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function findById($id) {
        $query = $this->createQuery();
        $query->matching(
           $query->equals('Persistence_Object_Identifier', $id)
        );
        $data= $query->execute();
        return $data;
    }

    public function findAllItems() {

        $query = $this->entityManager->createQueryBuilder();
        $data = $query->select('p')->from('\evenpro\vincenti\Domain\Model\Producto', 'p')
         ->getQuery()->execute();

         return $data  ;

    }

    public function findPublicados() {

        $query = $this->entityManager->createQueryBuilder();
        $data = $query->select('p')->from('\evenpro\vincenti\Domain\Model\Producto', 'p')
         ->where("p.publicar='1'")
         ->getQuery()->execute();

         return $data  ;

    }

    public function findByIdImage($idImage) {
        $query = $this->createQuery();
        $query->matching(
           $query->equals('originalResource',"'".$idImage."'")
        );
        $query->getSql();
        $data= $query->execute();
        return $data;
    }


}
