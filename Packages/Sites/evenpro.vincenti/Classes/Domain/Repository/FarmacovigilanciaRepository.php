<?php
namespace evenpro\vincenti\Domain\Repository;

/*
 * This file is part of the form.poster package.
 */

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;
/**
 * @Flow\Scope("singleton")
 */
class FarmacovigilanciaRepository extends Repository
{

     /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;


}
