<?php
namespace evenpro\vincenti\Domain\Repository;

use Neos\Flow\Annotations as Flow;
use Neos\Flow\Persistence\Repository;
use Neos\Flow\Persistence\QueryInterface;
use Neos\Flow\Persistence\QueryResultInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Neos\Flow\Persistence\PersistenceManagerInterface;

/*
 * This file is part of the evenpro.vincenti package.
 */

/**
 * @Flow\Scope("singleton")
 */
class CovidRepository extends Repository
{
    /**
     * @Flow\Inject
     * @var EntityManagerInterface
     */
    protected $entityManager;

    public function findById($id) {  
         $query = $this->createQuery(); 
         $query->matching(
            $query->equals('Persistence_Object_Identifier', $id) 
         );
         $data= $query->execute();
        return $data;   
    }

    public function findPublicados() {

        $query = $this->entityManager->createQueryBuilder();
        $data = $query->select('n')->from('\evenpro\vincenti\Domain\Model\Covid', 'n')
         ->where("n.vigente='1'")
         ->getQuery()->execute();

         return $data  ;
    }

    public function getAllOrderByFecha() {
        $query = $this->entityManager->createQueryBuilder();
        
        $data = $query->select('c')->from('\evenpro\vincenti\Domain\Model\Covid', 'c')
            ->orderBy('c.fecha', 'DESC')
            //->setMaxResults(20)
            ->getQuery()->execute();
        
            return $data;
    }
}
