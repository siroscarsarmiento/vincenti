<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the poster package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;


/**
 * @Flow\Entity
 */
class Categoria
{

    /**
     * @ORM\OneToMany(mappedBy="categoria")
     * @var Collection<Producto>
     */
    protected $producto;

    /**
     * @var string
    */
    protected $categoria;

    /**
     * @var string
    */
    protected $categoriaEn;

    /**
      * @var integer
      */
    protected $publicar;
    
    /**
     * @var \DateTime
     */
    protected $fecha;



    /**
     * @return string
     */
    public function getCategoria()
    {
        return $this->categoria;
    }

    /**
     * @param string $categoria
     * @return void
     */
    public function setCategoria($categoria)
    {
        $this->categoria = $categoria;
    }

    /**
     * @return string
     */
    public function getCategoriaEn()
    {
        return $this->categoriaEn;
    }

    /**
     * @param string $categoriaEn
     * @return void
     */
    public function setCategoriaEn($categoriaEn)
    {
        $this->categoriaEn = $categoriaEn;
    }

    /**
     * @return integer
     */
    public function getPublicar()
    {
        return $this->publicar;
    }

    /**
     * @param integer $publicar
     * @return void
     */
    public function setPublicar($publicar)
    {
        $this->publicar = $publicar;
    }

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }


    /**
     * @return string
     */
    public function getPersistence_Object_Identifier()
    {
        return $this->Persistence_Object_Identifier;
    }
    


}
