<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the evenpro.cinex package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Noticias
{
    
    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $tituloEn;

    /**
     * @var string
     * @ORM\Column(length=500)
     */
    protected $resumen;

    /**
     * @var string
     * @ORM\Column(length=500)
     */
    protected $resumenEn;

    /**
     * @var string
     * @ORM\Column(length=9000)
     */
    protected $contenido;

    /**
     * @var string
     * @ORM\Column(length=9000)
     */
    protected $contenidoEn;

    /**
     * @var string
     */
    protected $tags;

    /**
     * @var string
     */
    protected $tagsEn;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var integer
     */
    protected $vigente;

    /**
     * @var \Neos\Flow\ResourceManagement\PersistentResource
     * @ORM\OneToOne
     */
    protected $originalResource;

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return void
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTituloEn()
    {
        return $this->tituloEn;
    }

    /**
     * @param string $tituloEn
     * @return void
     */
    public function setTituloEn($tituloEn)
    {
        $this->tituloEn = $tituloEn;
    }

    /**
     * @return string
     */
    public function getResumen()
    {
        return $this->resumen;
    }

    /**
     * @param string $resumen
     * @return void
     */
    public function setResumen($resumen)
    {
        $this->resumen = $resumen;
    }

    /**
     * @return string
     */
    public function getResumenEn()
    {
        return $this->resumenEn;
    }

    /**
     * @param string $resumenEn
     * @return void
     */
    public function setResumenEn($resumenEn)
    {
        $this->resumenEn = $resumenEn;
    }

    /**
     * @return string
     */
    public function getContenido()
    {
        return $this->contenido;
    }

    /**
     * @param string $contenido
     * @return void
     */
    public function setContenido($contenido)
    {
        $this->contenido = $contenido;
    }

    /**
     * @return string
     */
    public function getContenidoEn()
    {
        return $this->contenidoEn;
    }

    /**
     * @param string $contenidoEn
     * @return void
     */
    public function setContenidoEn($contenidoEn)
    {
        $this->contenidoEn = $contenidoEn;
    }

    /**
     * @return string
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * @param string $tags
     * @return void
     */
    public function setTags($tags)
    {
        $this->tags = $tags;
    }

    /**
     * @return string
     */
    public function getTagsEn()
    {
        return $this->tagsEn;
    }

    /**
     * @param string $tagsEn
     * @return void
     */
    public function setTagsEn($tagsEn)
    {
        $this->tagsEn = $tagsEn;
    }

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }

    /**
     * @return integer
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * @param integer $vigente
     * @return void
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;
    }


     /**
     * @return string
     */
    public function getPersistence_Object_Identifier()
    {
        return $this->Persistence_Object_Identifier;
    }
    
    /**
     * @param \Neos\Flow\ResourceManagement\PersistentResource $originalResource
     * @return void
     */
    public function setOriginalResource(\Neos\Flow\ResourceManagement\PersistentResource $originalResource) {
        $this->originalResource = $originalResource;
    }

    /**
     * @return \Neos\Flow\ResourceManagement\PersistentResource
     */
    public function getOriginalResource() {
        return $this->originalResource;
    }
}
