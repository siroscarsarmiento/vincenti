<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the poster package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;


/**
 * @Flow\Entity
 */
class Farmacovigilancia
{

    /**
     * @var string
     * @ORM\Column(length=50)
     */
    protected $nombre;

    /**
     * @var string
     * @ORM\Column(length=10)
     */
    protected $ci;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    protected $correo;

    /**
     * @var string
     * @ORM\Column(length=50)
     */
    protected $telefono;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    protected $medicamento;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    protected $presentacion;

    /**
     * @var string
     * @ORM\Column(length=200)
     */
    protected $reaccion;

    /**
     * @var string
     * @ORM\Column(length=1000)
     */
    protected $coments;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return void
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getCi()
    {
        return $this->ci;
    }

    /**
     * @param string $ci
     * @return void
     */
    public function setCi($ci)
    {
        $this->ci = $ci;
    }

    /**
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param string $correo
     * @return void
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     * @return void
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }
    
    /**
     * @return string
     */
    public function getMedicamento()
    {
        return $this->medicamento;
    }

    /**
     * @param string $medicamento
     * @return void
     */
    public function setMedicamento($medicamento)
    {
        $this->medicamento = $medicamento;
    }
    
    /**
     * @return string
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * @param string $presentacion
     * @return void
     */
    public function setPresentacion($presentacion)
    {
        $this->presentacion = $presentacion;
    }

    /**
     * @return string
     */
    public function getReaccion()
    {
        return $this->reaccion;
    }

    /**
     * @param string $reaccion
     * @return void
     */
    public function setReaccion($reaccion)
    {
        $this->reaccion = $reaccion;
    }

    /**
     * @return string
     */
    public function getComents()
    {
        return $this->coments;
    }

    /**
     * @param string $coments
     * @return void
     */
    public function setComents($coments)
    {
        $this->coments = $coments;
    }

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }


    /**
     * @return string
     */
    public function getPersistence_Object_Identifier()
    {
        return $this->Persistence_Object_Identifier;
    }
    


}
