<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the evenpro.cinex package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;

/**
 * @Flow\Entity
 */
class Covid
{
    
    /**
     * @var string
     */
    protected $titulo;

    /**
     * @var string
     */
    protected $tituloEn;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @var integer
     */
    protected $vigente;

    /**
     * @var \Neos\Flow\ResourceManagement\PersistentResource
     * @ORM\OneToOne
     */
    protected $originalResource;

    /**
     * @var \Neos\Flow\ResourceManagement\PersistentResource
     * @ORM\OneToOne
     */
    protected $iconoResource;

    /**
     * @var \Neos\Flow\ResourceManagement\PersistentResource
     * @ORM\OneToOne
     */
    protected $adjuntoResource;

    /**
     * @return string
     */
    public function getTitulo()
    {
        return $this->titulo;
    }

    /**
     * @param string $titulo
     * @return void
     */
    public function setTitulo($titulo)
    {
        $this->titulo = $titulo;
    }

    /**
     * @return string
     */
    public function getTituloEn()
    {
        return $this->tituloEn;
    }

    /**
     * @param string $tituloEn
     * @return void
     */
    public function setTituloEn($tituloEn)
    {
        $this->tituloEn = $tituloEn;
    }


    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }

    /**
     * @return integer
     */
    public function getVigente()
    {
        return $this->vigente;
    }

    /**
     * @param integer $vigente
     * @return void
     */
    public function setVigente($vigente)
    {
        $this->vigente = $vigente;
    }
    
    /**
     * @param \Neos\Flow\ResourceManagement\PersistentResource $originalResource
     * @return void
     */
    public function setOriginalResource(\Neos\Flow\ResourceManagement\PersistentResource $originalResource) {
        $this->originalResource = $originalResource;
    }

    /**
     * @return \Neos\Flow\ResourceManagement\PersistentResource
     */
    public function getOriginalResource() {
        return $this->originalResource;
    }

    /**
     * @param \Neos\Flow\ResourceManagement\PersistentResource $iconoResource
     * @return void
     */
    public function setIconoResource(\Neos\Flow\ResourceManagement\PersistentResource $iconoResource) {
        $this->iconoResource = $iconoResource;
    }

    /**
     * @return \Neos\Flow\ResourceManagement\PersistentResource
     */
    public function getIconoResource() {
        return $this->iconoResource;
    }

    /**
     * @param \Neos\Flow\ResourceManagement\PersistentResource $adjuntoResource
     * @return void
     */
    public function setAdjuntoResource(\Neos\Flow\ResourceManagement\PersistentResource $adjuntoResource) {
        $this->adjuntoResource = $adjuntoResource;
    }

    /**
     * @return \Neos\Flow\ResourceManagement\PersistentResource
     */
    public function getAdjuntoResource() {
        return $this->adjuntoResource;
    }

     /**
     * @return string
     */
     public function getPersistence_Object_Identifier()
     {
         return $this->Persistence_Object_Identifier;
     }
}