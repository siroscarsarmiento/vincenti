<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the poster package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use evenpro\vincenti\Domain\Model\Categoria;
/**
 * @Flow\Entity
 */
class Producto
{

    /**
      * @var string
    */
    protected $nombre;


    /**
     * @ORM\ManyToOne(inversedBy="producto")
     * @var Categoria
    */
    protected $categoria;

    /**
      * @var string
    */
    protected $nombreEn;

    /**
      * @var string
    */
    protected $descripcion;

    /**
      * @var string
    */
    protected $descripcionEn;

    /**
      * @var string
    */
    protected $composicion;

    /**
      * @var string
    */
    protected $composicionEn;

    /**
      * @var string
    */
    protected $viaAdministracion;

    /**
      * @var string
    */
    protected $viaAdministracionEn;

    /**
     * @var string
    */
    protected $indicaciones;

    /**
     * @var string
    */
    protected $indicacionesEn;
      
    /**
     * @var string
    */
    protected $presentacion;

    /**
     * @var string
    */
    protected $presentacionEn;

    /**
     * @var string
    */
    protected $posologia;

    /**
     * @var string
    */
    protected $posologiaEn;

    /**
     * @var string
    */
    protected $division;

    /**
     * @var string
    */
    protected $divisionEn;

    /**
      * @var \Neos\Flow\ResourceManagement\PersistentResource
      * @ORM\OneToOne
      */
    protected $originalResource;

    /**
      * @var integer
      */
    protected $publicar;

    
    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return void
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

        /**
     * @return string
     */
    public function getNombreEn()
    {
        return $this->nombreEn;
    }

    /**
     * @param string $nombreEn
     * @return void
     */
    public function setNombreEn($nombreEn)
    {
        $this->nombreEn = $nombreEn;
    }


    /**
     * @return string
     */
    public function getDescripcion()
    {
        return $this->descripcion;
    }

    /**
     * @param string $descripcion
     * @return void
     */
    public function setDescripcion($descripcion)
    {
        $this->descripcion = $descripcion;
    }


    /**
     * @return string
     */
    public function getDescripcionEn()
    {
        return $this->descripcionEn;
    }

    /**
     * @param string $descripcionEn
     * @return void
     */
    public function setDescripcionEn($descripcionEn)
    {
        $this->descripcionEn = $descripcionEn;
    }

    /**
     * @return string
     */
    public function getComposicion()
    {
        return $this->composicion;
    }

    /**
     * @param string $composicion
     * @return void
     */
    public function setComposicion($composicion)
    {
        $this->composicion = $composicion;
    }

    /**
     * @return string
     */
    public function getComposicionEn()
    {
        return $this->composicionEn;
    }

    /**
     * @param string $composicionEn
     * @return void
     */
    public function setComposicionEn($composicionEn)
    {
        $this->composicionEn = $composicionEn;
    }


    /**
     * @return string
     */
    public function getViaAdministracion()
    {
        return $this->viaAdministracion;
    }

    /**
     * @param string $viaAdministracion
     * @return void
     */
    public function setViaAdministracion($viaAdministracion)
    {
        $this->viaAdministracion = $viaAdministracion;
    }

    /**
     * @return string
     */
    public function getViaAdministracionEn()
    {
        return $this->viaAdministracionEn;
    }

    /**
     * @param string $viaAdministracionEn
     * @return void
     */
    public function setViaAdministracionEn($viaAdministracionEn)
    {
        $this->viaAdministracionEn = $viaAdministracionEn;
    }

    /**
     * @return string
     */
    public function getIndicaciones()
    {
        return $this->indicaciones;
    }

    /**
     * @param string $indicaciones
     * @return void
     */
    public function setIndicaciones($indicaciones)
    {
        $this->indicaciones = $indicaciones;
    }

        /**
     * @return string
     */
    public function getIndicacionesEn()
    {
        return $this->indicacionesEn;
    }

    /**
     * @param string $indicacionesEn
     * @return void
     */
    public function setIndicacionesEn($indicacionesEn)
    {
        $this->indicacionesEn = $indicacionesEn;
    }

    /**
     * @return string
     */
    public function getPosologia()
    {
        return $this->posologia;
    }

    /**
     * @param string $posologia
     * @return void
     */
    public function setPosologia($posologia)
    {
        $this->posologia = $posologia;
    }

    /**
     * @return string
     */
    public function getPosologiaEn()
    {
        return $this->posologiaEn;
    }

    /**
     * @param string $posologiaEn
     * @return void
     */
    public function setPosologiaEn($posologiaEn)
    {
        $this->posologiaEn = $posologiaEn;
    }

    /**
     * @return string
     */
    public function getPresentacion()
    {
        return $this->presentacion;
    }

    /**
     * @param string $presentacion
     * @return void
     */
    public function setPresentacion($presentacion)
    {
        $this->presentacion = $presentacion;
    }

    /**
     * @return string
     */
    public function getPresentacionEn()
    {
        return $this->presentacionEn;
    }

    /**
     * @param string $presentacionEn
     * @return void
     */
    public function setPresentacionEn($presentacionEn)
    {
        $this->presentacionEn = $presentacionEn;
    }

    /**
     * @return string
     */
    public function getDivision()
    {
        return $this->division;
    }

    /**
     * @param string $division
     * @return void
     */
    public function setDivision($division)
    {
        $this->division = $division;
    }

    /**
     * @return string
     */
    public function getDivisionEn()
    {
        return $this->divisionEn;
    }

    /**
     * @param string $divisionEn
     * @return void
     */
    public function setDivisionEn($divisionEn)
    {
        $this->divisionEn = $divisionEn;
    }

    /**
     * @return integer
     */
    public function getPublicar()
    {
        return $this->publicar;
    }

    /**
     * @param integer $publicar
     * @return void
     */
    public function setPublicar($publicar)
    {
        $this->publicar = $publicar;
    }

    
    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }



    /**
     * @param \Neos\Flow\ResourceManagement\PersistentResource $originalResource
     * @return void
     */
    public function setOriginalResource(\Neos\Flow\ResourceManagement\PersistentResource $originalResource) {
        $this->originalResource = $originalResource;
    }

    /**
     * @return \Neos\Flow\ResourceManagement\PersistentResource
     */
    public function getOriginalResource() {
        return $this->originalResource;
    }

    /**
     * @return string
     */
    public function getPersistence_Object_Identifier()
    {
        return $this->Persistence_Object_Identifier;
    }



    /**
     * @return Colletion
    */
    public function getCategoria()
    {
        return $this->categoria;
    }


    /**
     * @param \evenpro\vincenti\Domain\Model\Categoria $categoria
     * @return void
     */
    public function setCategoria(Categoria $categoria)
    {
        $this->categoria = $categoria;
    }

    /**
    * @param Categoria $categoria
    * @return void
    */
    public function addCategoria(Categoria $categoria)
    {
        $this->categoria->add($categoria);
    }


}
