<?php
namespace evenpro\vincenti\Domain\Model;

/*
 * This file is part of the poster package.
 */

use Neos\Flow\Annotations as Flow;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;


/**
 * @Flow\Entity
 */
class Contacto
{

    /**
     * @var string
     * @ORM\Column(length=50)
     */
    protected $nombre;

    /**
     * @var string
     * @ORM\Column(length=100)
     */
    protected $correo;

    /**
     * @var string
     * @ORM\Column(length=50)
     */
    protected $telefono;

    /**
     * @var string
     * @ORM\Column(length=500)
     */
    protected $mensaje;

    /**
     * @var \DateTime
     */
    protected $fecha;

    /**
     * @return string
     */
    public function getNombre()
    {
        return $this->nombre;
    }

    /**
     * @param string $nombre
     * @return void
     */
    public function setNombre($nombre)
    {
        $this->nombre = $nombre;
    }

    /**
     * @return string
     */
    public function getCorreo()
    {
        return $this->correo;
    }

    /**
     * @param string $correo
     * @return void
     */
    public function setCorreo($correo)
    {
        $this->correo = $correo;
    }

    /**
     * @return string
     */
    public function getTelefono()
    {
        return $this->telefono;
    }

    /**
     * @param string $telefono
     * @return void
     */
    public function setTelefono($telefono)
    {
        $this->telefono = $telefono;
    }
    
    /**
     * @return string
     */
    public function getMensaje()
    {
        return $this->mensaje;
    }

    /**
     * @param string $mensaje
     * @return void
     */
    public function setMensaje($mensaje)
    {
        $this->mensaje = $mensaje;
    }
 

    /**
     * @return \DateTime
     */
    public function getFecha()
    {
        return $this->fecha;
    }

    /**
     * @ORM\PrePersist 
     * @return void
     */
    public function setFecha()
    {
        $this->fecha =  new \DateTime();
    }


    /**
     * @return string
     */
    public function getPersistence_Object_Identifier()
    {
        return $this->Persistence_Object_Identifier;
    }
    


}
